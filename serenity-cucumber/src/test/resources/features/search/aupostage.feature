Feature: AU Postage Calculator

  Scenario Outline: Calculate International Postage
    Given user has "eaf3799d-1a79-4973-a8f4-2df13f30e7e0" as apikey to AU POST API
    When user enters <country_code>, <weight>, <service_code> as countrycode,parcel_weight, servicecode
    Then API returns <total_cost> as total cost for the package

    Examples: 
      | country_code | weight | service_code                   | total_cost |
      | "AL"         | "1.5"  | "INT_PARCEL_AIR_OWN_PACKAGING" | "59.90"    |
      | "NZ"         | "2"    | "INT_PARCEL_EXP_OWN_PACKAGING" | "50.60"    |
      | "IN"         | "3"    | "INT_PARCEL_STD_OWN_PACKAGING" | "65.80"    |
