Feature: MLC Life

  Scenario: Navigate to Lifeview Demo page
    Given user is on MLCLife Home page
    When user hovers on Partnering With Us
    And user clicks on Lifeview
    Then user should be navigated to "LifeView" Page
    And "Home Partnering with us Superannuation funds LifeView" should be present in the Breadcrumb
    When user clicks on Request a Demo link
    Then user should navigate to "LifeView Demo | MLC Life Insurance" Page
    And user should be able to enter "Paul Smyth" as name
    And user should be able to enter "MLC Life" as Company
    And user should be able to enter "paul.smyth@mlcl.com.au" as email
    And user should be able to enter "0456123789" as phone
    And user should be able to enter "09-Apr-2020" as preferred contact date
    And user should be able to enter "PM" as preferred contact time
    And user should be able to enter "Please have vehicle insurance details ready for VIC state before calling" as Required Details
