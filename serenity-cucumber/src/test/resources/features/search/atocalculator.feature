Feature: ATO Tax Calculator

  Scenario Outline: ATO Tax Calculator for Full Year
    Given user is on ATO Tax calculator page
    When user selects <varincomeyear> as income year
    And user enters <vartaxableincome> as taxable income
    And user selects <varresstatus> as residency status
    And user clicks submit button
    Then result should display <varcalctax> as total tax

    Examples: 
      | varincomeyear | vartaxableincome | varresstatus                 | varcalctax   |
      | "2018-19"     | "90000"          | "Resident for full year"     | "$20,797.00" |
      | "2016-17"     | "80000"          | "Non-resident for full year" | "$26,000.00" |

  Scenario Outline: ATO Tax Calculator for Partial Year
    Given user is on ATO Tax calculator page
    When user selects <varincomeyear> as income year
    And user enters <vartaxableincome> as taxable income
    And user selects <varresstatus> as residency status
    And user selects <varresmonths> as resident months
    And user clicks submit button
    Then result should display <varcalctax> as total tax

    Examples: 
      | varincomeyear | vartaxableincome | varresstatus         | varresmonths | varcalctax   |
      | "2014-15"     | "70000"          | "Part-year resident" | "3"          | "$14,971.88" |
