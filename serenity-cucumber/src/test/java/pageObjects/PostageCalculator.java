package pageObjects;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import net.thucydides.core.annotations.Step;



public class PostageCalculator{
	
	String urlPrefix = "digitalapi.auspost.com.au";
	String postageTypesURL = "https://" + urlPrefix + "/postage/parcel/international/calculate.json?";
	String apiKey;
	HttpResponse response;
	
	@Step
	public void setapikey(String varapiKey) {
				apiKey = varapiKey;
	}
	
	@Step
	public void htmlsendrequest(String varcountrycode, String varweight, String varservicecode) throws Exception {
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("country_code", varcountrycode));
		params.add(new BasicNameValuePair("weight", varweight));
		params.add(new BasicNameValuePair("service_code", varservicecode));
		String query = URLEncodedUtils.format(params, "UTF-8");
		
			HttpUriRequest httpGet = new HttpGet(postageTypesURL + query);
			httpGet.setHeader("AUTH-KEY", apiKey);
			response = HttpClientBuilder.create().build().execute(httpGet);
			// Check the response: if the body is empty then an error occurred
	}
			
			
	@Step
	public void verifyhtmlresponse(String strTotalCost) throws Exception {
		if(response.getStatusLine().getStatusCode() != 200){
			  throw new Exception("Error: '" + response.getStatusLine().getReasonPhrase() + "' - Code: " + response.getStatusLine().getStatusCode());
			}
			
			   String jsonResponse = EntityUtils.toString(response.getEntity());	   
			   JSONParser parser = new JSONParser();
			   JSONObject jsonObject = (JSONObject) parser.parse(jsonResponse);
			   String strjsonresponse = jsonObject.toJSONString();
			   System.out.println("********************************************************************************************************************************");
			   System.out.println("********************************************************************************************************************************");
			   System.out.println("Total_cost for the postage = " + jsonObject.get("postage_result"));
			   System.out.println("********************************************************************************************************************************");
			   System.out.println("********************************************************************************************************************************");
			   assertTrue(strjsonresponse.contains(strTotalCost));
	}
}

