package pageObjects;

import org.testng.Assert;
import org.openqa.selenium.By;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

public class MLCLifeViewDemo extends PageObject{

	@Step
	public void verify_LifeViewDemoPage(String strExpectedPageTitle) {
		String stractualPageTitle = this.getDriver().getTitle();
		Assert.assertTrue(stractualPageTitle.contains(strExpectedPageTitle));
		}

	@Step
	public void enter_Name(String varname) {
		$(By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_0__Value")).type(varname);
	}
	
	@Step
	public void enter_Company(String varcompany){
		$(By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_1__Value")).type(varcompany);
	}
	
	@Step
	public void enter_Email(String varemail) {
		$(By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_2__Value")).type(varemail);
	}

	@Step
	public void enter_Phone(String varphone) {
		$(By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_3__Value")).type(varphone);
	}
	
	@Step
	public void enter_PDate(String varprefdate) {
		$(By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_4__Value")).type(varprefdate);
	}
	
	@Step
	public void enter_PTime(String varpreftime) {
		if(varpreftime.equalsIgnoreCase("PM")) {
			$(By.xpath("//input[@value='PM']")).click();			
		} else if(varpreftime.equalsIgnoreCase("AM")) {
			$(By.xpath("//input[@value='AM']")).click();			
		}
	}
	
	@Step
	public void enter_Req_Details(String varreqdetails) {
		$(By.id("wffm0635571898ed434b8db4317b0d7a8d19_Sections_0__Fields_6__Value")).type(varreqdetails);
		
	}
	
}
