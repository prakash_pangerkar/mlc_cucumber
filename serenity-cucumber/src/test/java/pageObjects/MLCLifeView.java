package pageObjects;

import org.openqa.selenium.By;
import org.testng.Assert;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

public class MLCLifeView extends PageObject{
	
	@Step
	public void verify_LifeViewPage(String strExpectedPageTitle) {
		String stractualPageTitle = this.getDriver().getTitle();
		Assert.assertTrue(stractualPageTitle.contains(strExpectedPageTitle));
		}
	
	@Step
	public void verify_breadcrumb(String strexpectedbreadcrumb) {
		String stractualbreadcrumb = $(By.xpath("//div[@class='breadcrumbs']")).getText();
		Assert.assertEquals(stractualbreadcrumb, strexpectedbreadcrumb);		
	}
	
	@Step
	public void Click_RequestDemo() {
		$(By.linkText("Request a demo")).click();
	}
	
}
