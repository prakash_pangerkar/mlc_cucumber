package pageObjects;

import org.openqa.selenium.By;
import org.testng.Assert;

import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

public class ATO_Calculator extends PageObject{
		
	//Select Income Year From Dropdown
	@Step
	public void selectincomeyear(String varincomeyear)
	{
		$(By.xpath("//select[@name='ddl-financialYear']")).waitUntilVisible();
		$(By.xpath("//select[@name='ddl-financialYear']")).selectByVisibleText(varincomeyear);
	}
	
	//Enter the taxable income amount
	@Step
	public void entertaxableincome(String vartaxableincome)
	{
		$(By.name("texttaxIncomeAmt")).type(vartaxableincome);
	}
	
	//Select the residency status
	@Step
	public void selectresidencystatus(String varresstatus)
	{
		switch(varresstatus) 
		{
		case "Resident for full year" : 
			$(By.xpath("//input[@value='resident']/../label/span[2]")).click();
			break;
		case "Non-resident for full year": 
			$(By.xpath("//input[@value='nonResident']/../label/span[2]")).click();
			break;
		case "Part-year resident": 
			$(By.xpath("//input[@value='partYearResident']/../label/span[2]")).click();			
			break;
		}	
	}

	//Select the resident months for Part Year
	@Step
	public void selectresidentmonths(String strresmonths) {
		$(By.xpath("//select[@name='ddl-residentPartYearMonths']")).selectByVisibleText(strresmonths);		
	}
	
	//Click the submit button
	@Step
	public void click_submit() {
		$(By.id("bnav-n1-btn4")).click();
	}

	//Verify the calculated Tax
	@Step
	public void verify_tax(String strexpectedtax) {
		String stractualtax = $(By.xpath("//*[text()='The estimated tax on your taxable income is ']/span")).getText();
		System.out.println("The estimated tax amount calculated is " + stractualtax);
		Assert.assertEquals(stractualtax, strexpectedtax);		
	}

	
}
