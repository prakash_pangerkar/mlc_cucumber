package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import net.thucydides.core.annotations.Step;
import net.thucydides.core.pages.PageObject;

public class MLCHome extends PageObject{

	@Step
	public void click_LifeView(){
	    int attempts = 0;
	    while(attempts < 2) {
	        try {
	    		$(By.linkText("LifeView")).waitUntilVisible();
	    		$(By.linkText("LifeView")).click();
	            break;
	        } catch(NoSuchElementException e) {
	        }
	        attempts++;
	    }
	}

	@Step
	public void click_Partnering() {	
	    int attempts = 0;
	    while(attempts < 2) {
	        try {
				WebElement Partnering = $(By.xpath(".//a[@class='nav-item-title' and @href='/partnering-with-us']/..")).findElement(By.xpath(".//a[@href='/partnering-with-us']"));
				Partnering.click();
				Actions actions = new Actions(this.getDriver());
				actions.moveToElement(Partnering).build().perform();
	            break;
	        } catch(StaleElementReferenceException e) {
	        }
	        attempts++;
	    }
	}

}
