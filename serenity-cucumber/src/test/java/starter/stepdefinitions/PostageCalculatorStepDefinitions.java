package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pageObjects.PostageCalculator;

public class PostageCalculatorStepDefinitions 
{
	@Steps
	PostageCalculator postage;
	
	@Given("user has {string} as apikey to AU POST API")
	public void user_has_access_to_AU_POST_API(String varapikey) {
		postage.setapikey(varapikey);
	}

	@When("user enters {string}, {string}, {string} as countrycode,parcel_weight, servicecode")
	public void user_enters_as_countrycode_parcel_weight_servicecode(String varcountrycode, String varweight, String varservicecode) throws Exception {
	    postage.htmlsendrequest(varcountrycode, varweight, varservicecode);
	}

	@Then("API returns {string} as total cost for the package")
	public void api_returns_as_total_cost_for_the_package(String varTotalCost) throws Exception {
		varTotalCost = "\"total_cost\":\""+varTotalCost+"\"";
	    postage.verifyhtmlresponse(varTotalCost);
	    
	}


}
