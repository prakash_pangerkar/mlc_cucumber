package starter.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pageObjects.ATO_Calculator;

public class ATOCalculatorStepDefinitions {

	String strUrl = "https://www.ato.gov.au/Calculators-and-tools/Host/?anchor=STC&anchor=STC#STC/questions";

	@Steps
	ATO_Calculator Taxcalculator;
	

	@Given("user is on ATO Tax calculator page")
	public void user_is_on_ATO_Tax_calculator_page() {
		Taxcalculator.openUrl(strUrl);
	}

	@When("user selects {string} as income year")
	public void user_selects_income_year(String varincomeyear) {
		Taxcalculator.selectincomeyear(varincomeyear);
	}

	@When("user enters {string} as taxable income")
	public void user_enters_taxable_income(String vartaxableincome) {
		Taxcalculator.entertaxableincome(vartaxableincome);
	}

	@When("user selects {string} as residency status")
	public void user_selects_residency_status(String varresstatus) {
		Taxcalculator.selectresidencystatus(varresstatus);
	}

	@When("user selects {string} as resident months")
	public void user_selects_as_resident_months(String varresmonths) {
		Taxcalculator.selectresidentmonths(varresmonths);
	}
	
	@When("user clicks submit button")
	public void user_clicks_submit_button() {
		Taxcalculator.click_submit();
	}

	@Then("result should display {string} as total tax")
	public void total_tax_should_be_displayed(String varcalctax) {
		Taxcalculator.verify_tax(varcalctax);
		Taxcalculator.getDriver().close();
	}
	
}
