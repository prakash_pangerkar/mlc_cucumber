package starter.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import pageObjects.MLCHome;
import pageObjects.MLCLifeView;
import pageObjects.MLCLifeViewDemo;

public class MlcLifeStepDefinitions {
	String strUrl = "https://www.mlcinsurance.com.au/";
	
	@Steps
	MLCHome home;
	
	@Steps
	MLCLifeView lifeview;
	
	@Steps
	MLCLifeViewDemo demo;

	@Given("user is on MLCLife Home page")
	public void user_is_on_MLCLife_Home_page() {
		home.openUrl(strUrl );
	}

	@When("user hovers on Partnering With Us")
	public void user_hovers_on_Partnering_With_Us() {
		home.click_Partnering();
	}

	@When("user clicks on Lifeview")
	public void user_clicks_on_Lifeview() {
		home.click_LifeView();
	}

	@Then("user should be navigated to {string} Page")
	public void user_should_navigate_to_LifeView_Page(String LifeView) {
		lifeview.verify_LifeViewPage(LifeView);
	}

	@And("{string} should be present in the Breadcrumb")
	public void lifeview_should_be_present_in_Breadcrumb(String string) {
		lifeview.verify_breadcrumb(string);
	}

	@When("user clicks on Request a Demo link")
	public void user_clicks_on_Request_a_Demo_link() {
		lifeview.Click_RequestDemo();
	}

	@Then("user should navigate to {string} Page")
	public void user_should_navigate_to_Page(String string) {
		demo.verify_LifeViewDemoPage(string);
	}

	@And("user should be able to enter {string} as name")
	public void user_should_be_able_to_enter_as_name(String varname) {
		demo.enter_Name(varname);
	}

	@And("user should be able to enter {string} as Company")
	public void user_should_be_able_to_enter_as_Company(String varcompany) {
		demo.enter_Company(varcompany);
	}

	@And("user should be able to enter {string} as email")
	public void user_should_be_able_to_enter_as_email(String varemail) {
		demo.enter_Email(varemail);
	}

	@And("user should be able to enter {string} as phone")
	public void user_should_be_able_to_enter_as_phone(String varphone) {
		demo.enter_Phone(varphone);
	}

	@And("user should be able to enter {string} as preferred contact date")
	public void user_should_be_able_to_enter_as_preferred_contact_date(String varprefdate) {
		demo.enter_PDate(varprefdate);
	}

	@And("user should be able to enter {string} as preferred contact time")
	public void user_should_be_able_to_enter_as_preferred_contact_time(String varpreftime) {
		demo.enter_PTime(varpreftime);
	}

	@And("user should be able to enter {string} as Required Details")
	public void user_should_be_able_to_enter_as_Required_Details(String varreqdetails) throws InterruptedException {
		demo.enter_Req_Details(varreqdetails);
		Thread.sleep(5000);
	}

}
